package com.example.springsecuritydemo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author will.tuo
 * @date 2021/8/18 13:37
 */
@RestController
public class TestController {
    @GetMapping("hello")
    public String hello(){
        return "hfds";
    }
}
