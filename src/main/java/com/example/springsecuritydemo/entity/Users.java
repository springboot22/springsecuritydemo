package com.example.springsecuritydemo.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author will.tuo
 * @date 2021/8/18 14:37
 */
@Data
@TableName("user")
public class Users {
    private Integer id;
    private String username;
    private String password;
    private String realname;
}
