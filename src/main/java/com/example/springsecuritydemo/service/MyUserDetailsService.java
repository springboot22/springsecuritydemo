package com.example.springsecuritydemo.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.springsecuritydemo.entity.Users;
import com.example.springsecuritydemo.mappepr.UsersMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author will.tuo
 * @date 2021/8/18 14:20
 *
 * @Service("userDetailsService") 这里的名称和Security2Config ->configure->auth.userDetailsService(userDetailsService);参数名一样
 */
@Service("userDetailsService")
public class MyUserDetailsService implements UserDetailsService {
    @Autowired
    private UsersMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //构造查询
        QueryWrapper<Users> wrapper = new QueryWrapper<>();
        wrapper.eq("username",username);
        //执行查询
        Users users = userMapper.selectOne(wrapper);
        if(users==null){
            //认证失败
            throw new UsernameNotFoundException("用户名不存在");
        }
        List<GrantedAuthority> auths = AuthorityUtils.commaSeparatedStringToAuthorityList("role");
        //从数据库查询中返回user对象，得到用户名密码
        User user = new User(users.getUsername(),new BCryptPasswordEncoder().encode(users.getPassword()),auths);
        System.out.println(user.toString());
        return user;
    }
}
