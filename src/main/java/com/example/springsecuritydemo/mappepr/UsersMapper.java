package com.example.springsecuritydemo.mappepr;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.springsecuritydemo.entity.Users;
import org.springframework.stereotype.Repository;

/**
 * @author will.tuo
 * @date 2021/8/18 14:38
 */
@Repository
public interface UsersMapper extends BaseMapper<Users> {
}
