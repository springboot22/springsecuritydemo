package com.example.springsecuritydemo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author will.tuo
 * @date 2021/8/18 14:14
 */
@Configuration
public class Security2Config extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserDetailsService userDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        //指定用哪个userDetailsService实现类
        auth.userDetailsService(userDetailsService).passwordEncoder(password());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //页面权限设置
        http
        //自定义自己的登录页面
        .formLogin()
             //登录页面创建
            .loginPage("/home.html")
             //登录访问路径
            .loginProcessingUrl("/user/login")
             //登陆成功要跳转的路径
            .defaultSuccessUrl("/test/index")
        .and().authorizeRequests()
             //设置那些路径可以直接访问，不需要认证
             .antMatchers("/").permitAll()
                //有某个（admins）权限的用户才能访问
             .antMatchers("").hasAuthority("admins")
                //有其中一个权限的用户就能访问
                //源码会把多参数拼接在一起，再根据，分割，所以可以写一个字符串按照逗号分隔，也可以直接写多参数
              .antMatchers("").hasAnyAuthority("admins,manager","manfg")
                //用户有这个角色的可以访问
                //源码在比较的时候 会给role前面加一个ROLE_所以给用户设置权限的时候，需要手动给前面加上ROLE_
                //还有一个hasAnyRole方法 可以类比上面的
                .antMatchers("").hasRole("csamn")

        .anyRequest().authenticated()

         //设置token相关
        .and().rememberMe().tokenRepository()
        .tokenValiditySeconds()
        .userDetailsService(userDetailsService)
        //关闭 csrf防护
        .and().csrf().disable();

        //配置没有权限会自动跳转的页面
        http.exceptionHandling().accessDeniedPage("");

        //退出
        http.logout().logoutUrl("/logout")
                .logoutSuccessUrl("/ds/sdd").permitAll();
    }

    @Bean
    PasswordEncoder password(){
        return new BCryptPasswordEncoder();
    }
}
